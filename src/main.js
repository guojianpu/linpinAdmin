import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock' 
//富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
  
Vue.use(VueQuillEditor)
Vue.use(VueRouter)


import axios from 'axios'
import Qs from 'qs' 

import instans from "./api/api"
Vue.prototype.axios = instans;
Vue.prototype.qs = Qs;

// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'


Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path == '/login') {
    sessionStorage.removeItem('token');
  }
  let token = sessionStorage.getItem('token');

  // console.log(token)
  if (!token && to.path != '/login') {
    next({ path: '/login' })
  
  } else {
    next()
  }
})

// router.afterEach(transition => {
// NProgress.done();
// });

new Vue({
  el: '#app',
  // template: '<App/>',
  router,
  store,
  components: { App },
  render: h => h(App)
}).$mount('#app')

