import Login from './views/Login.vue' 
import Home from './views/Home.vue'  
import Homes from './views/Homes.vue'  
import Form from './views/nav1/Form.vue'
import user from './views/nav1/user.vue'
import Page4 from './views/nav2/Page4.vue'
import Page5 from './views/nav2/Page5.vue'
import Page6 from './views/nav3/Page6.vue'
import echarts from './views/charts/echarts.vue'
import Goods from './views/goods/index.vue'
import Wares from './views/goods/wares.vue'
import Withd from './views/withdrawal/with.vue'
import Withdrawal from './views/withdrawal/withdrawal.vue'
import Subsc from './views/subscriber/subscriber1.vue'
import Subscr from './views/subscriber/subscriber2.vue'
import Subscri from './views/subscriber/subscriber3.vue'
import Subscriber from './views/subscriber/subscriber4.vue'
import Capital from './views/capital/capital.vue'
import Consumption from './views/capital/consumption.vue'
import running from './views/capital/running.vue'
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    }, 
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-bar-chart',
        leaf: true, 
        children: [
            { path: '/Homes', component: Homes, name: '业务概括' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '用户管理',
        iconCls: 'el-icon-message', 
        children: [  
            { path: '/subscriber1', component: Subsc, name: '用户列表' },
            { path: '/subscriber2', component: Subscr, name: '任务列表' },
            { path: '/subscriber3', component: Subscri, name: '包裹列表' },
            // { path: '/subscriber4', component: Subscriber, name: '提现列表' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '站点管理',
        iconCls: 'el-icon-message', 
        children: [  
            { path: '/form', component: Form, name: '创建分站' },
            { path: '/user', component: user, name: '分站列表' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '业绩统计',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/page4', component: Page4, name: '营收日报' },
            { path: '/page5', component: Page5, name: '分站业绩' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '提现审核',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/with', component: Withd, name: '分站站长提现' },
            { path: '/withdrawal', component: Withdrawal, name: '分站用户提现' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '商品管理',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/index', component: Goods, name: '商品录入' },
            { path: '/wares', component: Wares, name: '商品列表' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-address-card',
        leaf: true, 
        children: [
            { path: '/page6', component: Page6, name: '查件处理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-bar-chart',
        leaf: true, 
        children: [
            { path: '/echarts', component: echarts, name: '成本设置' }
        ]
    },  
    {
        path: '/',
        component: Home,
        name: '资金明细',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/capital', component: Capital, name: '充值明细' },
            { path: '/consumption', component: Consumption, name: '消费明细' },
            { path: '/running', component: running,hidden: true, name: '流水' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;