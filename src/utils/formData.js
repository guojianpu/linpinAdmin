export function gettime(t){
  var _time=new Date(t);
  var   year=_time.getFullYear();//2017
  var   month=_time.getMonth()>8?(_time.getMonth()+1):'0'+(_time.getMonth()+1);//7
  var   date=_time.getDate()>9?_time.getDate():'0'+_time.getDate();//10
  var   hour=_time.getHours()>9?_time.getHours():'0'+_time.getHours();;//10
  var   minute=_time.getMinutes()>9?_time.getMinutes():'0'+_time.getMinutes();;//56
  var   second=_time.getSeconds()>9?_time.getSeconds():'0'+_time.getSeconds();;//15
  
  return   year+"-"+month+"-"+date+"   "+hour+":"+minute+":"+second;//这里自己按自己需要的格式拼接
}
